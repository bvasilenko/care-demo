import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { DatePagination } from 'app/models/core';
import { HttpService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { sortBy, descendingSorter } from 'app/utils';

interface PatientActivitiesDatasourceState {
    status: Status;
    activities: any[];
    hasMore: boolean;
}

@Injectable()
export class PatientActivitiesDatasource {

    private _store: Datasource<PatientActivitiesDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<PatientActivitiesDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get activities() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.activities);
    }

    get status() {
        return this._store.snapshot.status;
    }

    get hasMore() {
        return this._store.snapshot.hasMore;
    }

    get lastTimestamp() {
        const activities = this._store.snapshot.activities;
        const lastActivity = activities && activities.length && activities[activities.length - 1];
        return lastActivity && lastActivity.date;
    }

    getPage(patientId: string, pagination: DatePagination) {
        this._store.nextState(state => {
            if (!pagination.fromDate) {
                state.status = Status.Loading;
                state.activities = [];
            } else {
                state.status = Status.Updating;
            }
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        let url = `/patients/${patientId}/activities?limit=${pagination.limit}`;

        if (pagination.fromDate) {
            url = `${url}&fromDate=${pagination.fromDate}`;
        }

        this._request =
            this._http.get(url)
            .map(response => this._parseActivities(response.json()))
            .subscribe(
            (data) => {
                this._store.nextState(state => {

                    if (pagination.fromDate) {
                        state.activities = state.activities.concat(data);
                    } else {
                        state.activities = data;
                    }

                    state.hasMore = data.length === pagination.limit;
                    state.status = Status.Ready;
                });
            },
            error => {
                this._store.nextState(state => ({
                    status: Status.Failed,
                }));
            });
    }

    private _parseActivities(json) {
        return sortBy(json as any[], activity => activity.date || '', descendingSorter);
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as PatientActivitiesDatasourceState;
    }
}
