import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared';

import { AlertsDatasource } from './alerts.datasource';
import { KpisDatasource } from './kpis.datasource';
import { LiveFeedDatasource } from './livefeed.datasource';
import { PatientsDatasource } from './patients.datasource';
import { PatientActivitiesDatasource } from './patient-activities.datasource';
import { PatientDetailsDatasource } from './patient-details.datasource';
import { PatientHistoryDatasource } from './patient-history.datasource';
import { PatientTasksDatasource } from './patient-tasks.datasource';
import { PlaybooksDatasource } from './playbooks.datasource';
import { TasksDatasource } from './tasks.datasource';

@NgModule({
    imports: [
        SharedModule,
    ],
    providers: [
        AlertsDatasource,
        KpisDatasource,
        LiveFeedDatasource,
        PatientsDatasource,
        PatientActivitiesDatasource,
        PatientDetailsDatasource,
        PatientHistoryDatasource,
        PatientTasksDatasource,
        PlaybooksDatasource,
        TasksDatasource,
    ],
})

export class DatasourcesModule {}
