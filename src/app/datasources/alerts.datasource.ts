import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

import { Alert, AlertType } from 'app/models';
import { HttpService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { sortBy, descendingSorter } from 'app/utils';

interface AlertsResponse {
    data: any[],
}

interface AlertsDatasourceState {
    status: Status;
    alerts: Alert[];
}

@Injectable()
export class AlertsDatasource {

    private _store: Datasource<AlertsDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<AlertsDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get alerts() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.alerts);
    }

    get status() {
        return this._store.snapshot.status;
    }

    getAll() {
        this._store.nextState(state => {
            state.status = Status.Loading;
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request =
            this._http.get(`/alerts`)
                .map(response => this._parseAlerts(response.json()))
                .subscribe(
                alerts => {
                    this._store.nextState(state => {
                        state.alerts = sortBy(alerts, alert => alert.deadline_date, descendingSorter);
                        state.status = Status.Ready;
                    });
                },
                error => {
                    this._store.nextState(state => ({
                        status: Status.Failed,
                    }));
                });
    }

    private _parseAlerts(obj: AlertsResponse) {
        return obj.data.map(alert => {
            if (alert.type.type_id === AlertType.Call) {
                alert.message = alert.message.split('##name##')[1];
            }
            return alert as Alert;
        });
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as AlertsDatasourceState;
    }
}