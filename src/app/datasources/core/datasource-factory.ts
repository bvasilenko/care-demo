import {Injectable} from '@angular/core';
import {Datasource} from './datasource';

@Injectable()
export class DatasourceFactory<TState> {

    public new(initialState: TState) {
        return new Datasource<TState>(initialState);
    }

}