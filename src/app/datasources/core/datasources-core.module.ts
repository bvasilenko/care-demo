import { DatasourceFactory } from './datasource-factory';
import { NgModule } from '@angular/core';

@NgModule({
    providers: [
        DatasourceFactory,
    ],
})
export class DatasourcesCoreModule { }
