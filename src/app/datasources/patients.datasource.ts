import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Patient, PatientFilter, PatientDetailsResponse, TreatmentPlan } from 'app/models';
import { Pagination, Sorting } from 'app/models/core';
import { HttpService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { randomInt, groupBy, map2array } from 'app/utils';

interface PatientsDatasourceState {
    status: Status;
    data: Patient[];
    count: number;
    sorting: Sorting;
    filter: PatientFilter,
    hasMore: boolean;
}

interface PatientsResponse {
    count: number;
    data: PatientDetailsResponse.Patient[];
}

@Injectable()
export class PatientsDatasource {

    private _store: Datasource<PatientsDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<PatientsDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get patients() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.data);
    }

    get status() {
        return this._store.snapshot.status;
    }

    get count() {
        return this._store.snapshot.count;
    }

    get sorting() {
        return this._store.snapshot.sorting;
    }

    get filter() {
        return this._store.snapshot.filter;
    }

    get hasMore() {
        return this._store.snapshot.hasMore;
    }

    getPage(sorting: Sorting, filter: PatientFilter, pagination: Pagination) {

        this._store.nextState(state => ({
            status: Status.Loading,
            resources: [],
            sorting,
            filter,
        }));

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request =
            this._http.post('/patients', {
                offset: pagination.offset,
                limit: pagination.limit,
                sortby: sorting.sortBy,
                sortdesc: sorting.descending,
                filter,
            })
            .map(response => this._parsePatientsResponse(response.json() as PatientsResponse))
            .subscribe(
            ({data, count}) => {
                this._store.nextState(state => ({
                    status: Status.Ready,
                    data,
                    count,
                    sorting,
                    filter,
                    hasMore: pagination.limit === data.length,
                }));
            },
            error => {
                this._store.nextState(state => ({
                    status: Status.Failed,
                }));
            });
    }

    loadMore(pagination: Pagination) {

        this._store.nextState(state => {
            state.status = Status.Updating;
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request =
            this._http.post('/patients', {
                offset: pagination.offset,
                limit: pagination.limit,
                sortby: this.sorting.sortBy,
                sortdesc: this.sorting.descending,
                filter: this.filter,
            })
            .map(response => this._parsePatientsResponse(response.json() as PatientsResponse))
            .subscribe(
            ({data, count}) => {
                this._store.nextState(state => {
                    state.status = Status.Ready;
                    state.data = state.data.concat(data);
                    state.hasMore = data.length === pagination.limit;
                });
            },
            error => {
                this._store.nextState(state => ({
                    status: Status.Failed,
                }));
            });
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as PatientsDatasourceState;
    }

    private _parsePatientsResponse(response: PatientsResponse) {
        return {
            data: response.data.map(patient => this._parsePatient(patient)),
            count: response.count,  
        };
    }

    private _parsePatient(obj: PatientDetailsResponse.Patient) {
        const patient = obj as any as Patient;

        this._setRandomProperties(patient);

        return patient;
    }

    private _setRandomProperties(patient: Patient) {
        const seed = patient.id;
        Object.assign(patient, {
            treatmentPlans: this._getRandomTreatmentPlans(seed, patient.treatmentPlans),
            treatmentPlan: this._getRandomTreatment(seed, patient),
            playbook: this._getRandomPlaybook(seed),
        });
    }

    private _getRandomTreatmentPlans(seed: number, treatmentPlans: TreatmentPlan[]) {
        treatmentPlans.forEach((plan, i) => {
            plan.phases.forEach((phase, j) => {
                phase.items.forEach((item, k) => {
                    item.tooth = 19 + randomInt(3, seed ^ i ^ j ^ k);
                });
            });
        });
        return treatmentPlans;
    }

    private _getRandomTreatment(seed: number, patient: Patient) {

        if (seed % 11 === 0) {
            return;
        }

        const FAKE_TREATMENT_ITEMS = [
            'Crown',
            'Implants',
            'Ortho',
            'Bridge',
            'Priodontal',
            'Cavities'
        ];

        const treatmentPlan = patient.treatmentPlans[0];

        if (!treatmentPlan) {
            return {
                totalCost: null,
                items: null,
            }
        }

        return {
            totalCost: treatmentPlan.totalCost,
            items: treatmentPlan.phases
                .map((e, i) => FAKE_TREATMENT_ITEMS[(seed + i) % FAKE_TREATMENT_ITEMS.length])
        }
    }

    private _getRandomPlaybook(seed: number) {

        if (seed % 9 === 0) {
            return;
        }

        const FAKE_PLAYBOOKS = [
            'VIP',
            'New Patient',
            'Nurturing',
        ];

        return {
            name: FAKE_PLAYBOOKS[seed % FAKE_PLAYBOOKS.length]
        };
    }
}
