import { Injectable } from '@angular/core';

import { Kpi } from 'app/models';
import { HttpMockupService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { KPI_DATA_MOCK } from './kpis.datasource.mock';

interface KpiDatasourceState {
    status: Status;
    kpisLarge: Array<Kpi.KpiLargeTuple|Kpi.KpiLargeTriple>;
    kpis: Kpi.KpiSimple[];
}

@Injectable()
export class KpisDatasource {

    private _store: Datasource<KpiDatasourceState>;

    constructor(
        storeFactory: DatasourceFactory<KpiDatasourceState>,
        private _http: HttpMockupService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get kpisLarge() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.kpisLarge);
    }

    get kpis() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.kpis);
    }

    get status() {
        return this._store.snapshot.status;
    }

    async get() {
        this._store.nextState(state => {
            state.status = Status.Loading;
        });

        try {
            const {kpisLarge, kpis} = await this._http.get(`http://localhost/kpi-data`, undefined, KPI_DATA_MOCK());
            
            this._store.nextState(state => {

                state.kpisLarge = kpisLarge;
                state.kpis = kpis;

                state.status = Status.Ready;
            });
        } catch (error) {
            this._store.nextState(state => ({
                status: Status.Failed,
            }));
        }
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
            kpisLarge: [{}, {}, {}, {}],
            kpis: [{}, {}, {}, {}, {}, {}, {}, {}],
        } as KpiDatasourceState;
    }
}