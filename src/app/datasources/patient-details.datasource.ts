import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Patient, TreatmentPlan, PatientDetailsResponse } from 'app/models';
import { HttpService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { groupBy, map2array, randomInt } from 'app/utils';

interface PatientDetailsDatasourceState {
    status: Status;
    patientDetails: Patient;
}

@Injectable()
export class PatientDetailsDatasource {

    private _store: Datasource<PatientDetailsDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<PatientDetailsDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get patientDetails() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.patientDetails);
    }

    get status() {
        return this._store.snapshot.status;
    }

    get(id: string) {
        this._store.nextState(state => ({
            status: Status.Loading,
        }));

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request =
            this._http.get(`/patient/${id}`)
                .map(response => this._parsePatient(response.json() as PatientDetailsResponse.Patient))
                .subscribe(
                patientDetails => {
                    this._store.nextState(state => ({
                        status: Status.Ready,
                        patientDetails,
                    }));
                },
                error => {
                    this._store.nextState(state => ({
                        status: Status.Failed,
                    }));
                });
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as PatientDetailsDatasourceState;
    }

    private _parsePatient(obj: PatientDetailsResponse.Patient) {
        const patient = obj as any as Patient;

        this._setRandomProperties(patient);

        return patient;
    }

    private _parseTreatmentPlan(obj: PatientDetailsResponse.TreatmentPlan) {
        return {
            totalCost: obj.totalcost,
            name: obj.name,
            id: obj.id,
            phases: map2array(groupBy(obj.services, service => service.phase.toString()), (key, value) => ({
                phase: key,
                items: value,
                subtotal: value.reduce((sum, next) => sum + next.fee, 0),
            })),
        } as TreatmentPlan;
    }

    private _setRandomProperties(patient: Patient) {
        const seed = patient.id;
        Object.assign(patient, {
            contacts: [
                {
                    email: `${randomInt(9335, seed)}@gmail.com`,
                    type: 'email',
                },
                {
                    type: 'cell_phone',
                    phone: `${randomInt(9599399293, seed)}`,
                },
            ],
            score: {
                overall: randomInt(95, seed),
                relationship: randomInt(100, seed),
                responsive: randomInt(100, seed),
                financial: randomInt(100, seed),
                readiness: randomInt(100, seed),
            },
            treatmentPlans: this._getRandomTreatmentPlans(seed, patient.treatmentPlans),
            treatmentPlan: this._getRandomTreatment(seed, patient),
            playbook: this._getRandomPlaybook(seed),
        });
    }

    private _getRandomTreatmentPlans(seed: number, treatmentPlans: TreatmentPlan[]) {
        treatmentPlans.forEach((plan, i) => {
            plan.phases.forEach((phase, j) => {
                phase.items.forEach((item, k) => {
                    item.tooth = 19 + randomInt(3, seed ^ i ^ j ^ k);
                });
            });
        });
        return treatmentPlans;
    }

    private _getRandomTreatment(seed: number, patient: Patient) {

        if (seed % 11 === 0) {
            return;
        }

        const FAKE_TREATMENT_ITEMS = [
            'Crown',
            'Implants',
            'Ortho',
            'Bridge',
            'Priodontal',
            'Cavities'
        ];

        const treatmentPlan = patient.treatmentPlans[0];

        if (!treatmentPlan) {
            return {
                totalCost: null,
                items: null,
            }
        }

        return {
            totalCost: treatmentPlan.totalCost,
            items: treatmentPlan.phases
                .map((e, i) => FAKE_TREATMENT_ITEMS[(seed + i) % FAKE_TREATMENT_ITEMS.length])
        }
    }

    private _getRandomPlaybook(seed: number) {

        if (seed % 9 === 0) {
            return;
        }

        const FAKE_PLAYBOOKS = [
            'VIP',
            'New Patient',
            'Nurturing',
        ];

        return {
            name: FAKE_PLAYBOOKS[seed % FAKE_PLAYBOOKS.length]
        };
    }
}
