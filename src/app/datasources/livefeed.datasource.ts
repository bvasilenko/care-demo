import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

import { LiveFeed } from 'app/models';
import { HttpService } from 'app/services';
import { descendingSorter, sortBy } from 'app/utils';
import { Status, Datasource, DatasourceFactory } from './core';

interface EventsResponse {
    data: any[],
}

interface EventsDatasourceState {
    status: Status;
    events: LiveFeed[];
}

@Injectable()
export class LiveFeedDatasource {

    private _store: Datasource<EventsDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<EventsDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get events() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.events);
    }

    get status() {
        return this._store.snapshot.status;
    }

    getAll() {
        this._store.nextState(state => {
            state.status = Status.Loading;
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request = 
            this._http.get('/livefeed')
                .map(response => this._parseEvents(response.json()))
                .subscribe(
                    events => {
                        this._store.nextState(state => {
                            state.events = sortBy(events, event => event.time, descendingSorter);
                            state.status = Status.Ready;
                        });
                    },
                    error => {
                        this._store.nextState(state => ({
                            status: Status.Failed,
                        }));
                    });
    }

    private _parseEvents(obj: EventsResponse) {
        return obj.data.map(event => {
            event.time = new Date(event.time.replace('AM', '').replace('PM', ''));
            event.message = event.message.replace('##name##', '');
            return event as LiveFeed;
        });
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as EventsDatasourceState;
    }
}