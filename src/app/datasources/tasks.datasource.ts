import { Injectable } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { TaskDone } from 'app/models';
import { HttpService } from 'app/services';
import { sortBy, descendingSorter } from 'app/utils';
import { Status, Datasource, DatasourceFactory } from './core';

interface TasksResponse {
    data: any[],
}

interface TasksDatasourceState {
    status: Status;
    tasks: TaskDone[];
}

@Injectable()
export class TasksDatasource {

    private _store: Datasource<TasksDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<TasksDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get tasks() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.tasks);
    }

    get status() {
        return this._store.snapshot.status;
    }

    getAll() {
        this._store.nextState(state => {
            state.status = Status.Loading;
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request = 
            this._http.get(`/taskdone`)
                .map(response => this._parseTasks(response.json()))
                .subscribe(
                    tasks => {
                        this._store.nextState(state => {
                            state.tasks = sortBy(tasks, task => task.done_date, descendingSorter);
                            state.status = Status.Ready;
                        });
                    },
                    error => {
                        this._store.nextState(state => ({
                            status: Status.Failed,
                        }));
                    });
    }

    private _parseTasks(obj: TasksResponse) {
        return obj.data.map(task => {
            task.done_date = new Date(task.done_date.replace('AM', '').replace('PM', ''));
            task.message = task.message.split('##name##');
            return task as TaskDone;
        });
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as TasksDatasourceState;
    }
}