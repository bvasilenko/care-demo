import { Injectable } from '@angular/core';
import { Subscription, Observable } from 'rxjs/Rx';

import { Playbook } from 'app/models';
import { HttpService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { sortBy, ascendingSorter } from 'app/utils';

const PLAYBOOKS_MOCK = () => [
    { id: 100, name: 'Cosmetic' },
    { id: 102, name: 'Denture' },
    { id: 104, name: 'V.I.P.' },
    { id: 106, name: 'New patient' },
    { id: 108, name: 'Nurturing' },
] as Playbook[];

interface PlaybooksResponse {
    data: any[],
}

interface PlaybooksDatasourceState {
    status: Status;
    playbooks: Playbook[];
}

@Injectable()
export class PlaybooksDatasource {

    private _store: Datasource<PlaybooksDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<PlaybooksDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get playbooks() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.playbooks);
    }

    get status() {
        return this._store.snapshot.status;
    }

    getAll() {
        this._store.nextState(state => {
            state.status = Status.Loading;
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        this._request =
            // this._http.get(`/playbooks`)
            //     .map(response => this._parsePlaybooks(response.json()))
            Observable.of(PLAYBOOKS_MOCK()).delay(200)
                .subscribe(
                playbooks => {
                    this._store.nextState(state => {
                        state.playbooks = sortBy(playbooks, playbook => playbook.name, ascendingSorter);
                        state.status = Status.Ready;
                    });
                },
                error => {
                    this._store.nextState(state => ({
                        status: Status.Failed,
                    }));
                });
    }

    private _parsePlaybooks(obj: PlaybooksResponse) {
        return obj.data as Playbook[];
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as PlaybooksDatasourceState;
    }
}