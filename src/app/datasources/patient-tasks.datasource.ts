import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { DatePagination } from 'app/models/core';
import { HttpService } from 'app/services';
import { Status, Datasource, DatasourceFactory } from './core';
import { sortBy, descendingSorter } from 'app/utils';

interface PatientTasksDatasourceState {
    status: Status;
    tasks: any[];
    hasMore: boolean;
}

@Injectable()
export class PatientTasksDatasource {

    private _store: Datasource<PatientTasksDatasourceState>;
    private _request: Subscription;

    constructor(
        storeFactory: DatasourceFactory<PatientTasksDatasourceState>,
        private _http: HttpService,
    ) {
        this._store = storeFactory.new(this._emptyState());
    }

    get tasks() {
        return this._store.state.filter(state => state.status !== Status.NotInitialized).map(state => state.tasks);
    }

    get status() {
        return this._store.snapshot.status;
    }

    get hasMore() {
        return this._store.snapshot.hasMore;
    }

    get lastTimestamp() {
        const tasks = this._store.snapshot.tasks;
        const lastTask = tasks && tasks.length && tasks[tasks.length - 1];
        return lastTask && lastTask.date;
    }

    getPage(patientId: string, pagination: DatePagination) {
        this._store.nextState(state => {
            if (!pagination.fromDate) {
                state.status = Status.Loading;
                state.tasks = [];
            } else {
                state.status = Status.Updating;
            }
        });

        if (this._request) {
            this._request.unsubscribe();
        }

        let url = `/patients/${patientId}/tasks?limit=${pagination.limit}`;

        if (pagination.fromDate) {
            url = `${url}&fromDate=${pagination.fromDate}`;
        }

        this._request =
            this._http.get(url)
            .map(response => this._parseTasks(response.json()))
            .subscribe(
            (data) => {
                this._store.nextState(state => {

                    if (pagination.fromDate) {
                        state.tasks = state.tasks.concat(data);
                    } else {
                        state.tasks = data;
                    }

                    state.hasMore = data.length === pagination.limit;
                    state.status = Status.Ready;
                });
            },
            error => {
                this._store.nextState(state => ({
                    status: Status.Failed,
                }));
            });
    }

    private _parseTasks(json) {
        return sortBy(json as any[], task => task.date || '', descendingSorter);
    }

    private _emptyState() {
        return {
            status: Status.NotInitialized,
        } as PatientTasksDatasourceState;
    }
}
