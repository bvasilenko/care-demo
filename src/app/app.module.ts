import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { DatasourcesModule } from 'app/datasources';
import { ServicesModule } from 'app/services';
import { NavBarModule } from 'app/layout/navbar/navbar.module';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        routing,

        DatasourcesModule,
        ServicesModule,
        NavBarModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
