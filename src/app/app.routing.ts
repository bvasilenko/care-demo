import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: 'dashboard',
        loadChildren: 'app/route/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'patients',
        loadChildren: 'app/route/patients/patients.module#PatientsModule'
    },
    {
        path: 'patients/:id',
        loadChildren: 'app/route/patients/details/patient-details.module#PatientDetailsModule'
    },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
