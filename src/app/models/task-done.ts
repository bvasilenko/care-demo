export interface TaskDone {
    id: number;
    status: boolean;
    message: string[];
    client: {
        id: number;
        name: string;
    };
    done_date: Date;
}