export interface PatientFilter {
    searchText: string;
    patientType: string;
    lastVisit: {
        from: number;
        to: number;
    };
    treatmentTypeId: string;
    treatmentCost: {
        from: number;
        to: number;
    };
    playbookId: number;
}
