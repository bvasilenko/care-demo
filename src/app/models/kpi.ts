export namespace Kpi {

    export interface KpiValue {
        value: number;
        type: 'decimal'|'percent'|'currency';
        prefix?: string;
        postfix?: string;
    }
    export interface KpiSimple {
        title: string;
        status?: 'success'|'warning'|'danger';
        data: KpiValue;
        subdata: KpiValue;
        subdata2: KpiValue;
    }

    export interface KpiValueTuple { date: string; value1: number; value2: number; total: number; }
    export interface KpiSeriesTuple {
        label1: string;
        label2: string;
        days: KpiValueTuple[];
        weeks: KpiValueTuple[];
        months: KpiValueTuple[];
    };
    export interface KpiValueTriple { date: string; value1: number; value2: number; value3: number; total: number; }
    export interface KpiSeriesTriple {
        label1: string;
        label2: string;
        label3: string;
        days: KpiValueTriple[];
        weeks: KpiValueTriple[];
        months: KpiValueTriple[];
    };
    export interface KpiLargeTuple {
        type: 'tuple';
        title: string;
        data: KpiSeriesTuple;
        subdata: KpiValue;
    }
    export interface KpiLargeTriple {
        type: 'triple';
        title: string;
        data: KpiSeriesTriple;
        subdata: KpiValue;
    }

    export interface Response {
        kpisLarge: Array<KpiLargeTuple|KpiLargeTriple>,
        kpis: KpiSimple[];
    }
}