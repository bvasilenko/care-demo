export interface TreatmentPlan {
    id: number;
    name: string;
    totalCost: number;
    phases: TreatmentPlanPhase[];
}

export interface TreatmentPlanPhase {
    phase: string;
    items: TreatmentPlanItem[];
    subtotal: number;
}

export interface TreatmentPlanItem {
    id: number;
    fee: number;
    phase: number;
    description: string;
    serviceCode: string;
    tooth: number;
}
