import { Contact } from './contact';
import { TreatmentPlan } from './treatment';

export interface Patient {
    contacts: Contact[];
    firstName: string;
    treatmentPlans: TreatmentPlan[];
    hoh: boolean;
    sex: string;
    age: number;
    insuranceInfo: {
        remainingBenefits: number;
        remainingDeductible: number;
        insuranceCompanyName: string;
        groupNumber: number;
        resetMonth: number;
        employerName: string;
    },
    lastVisit: string;
    middleName: string;
    responsiblePartyId: number;
    intelliRank: number;
    middleInitial: string;
    address: {
        country: string;
        city: string;
        address: string;
        zipcode: string;
        state: string;
    },
    id: number;
    lastName: string;
    score: {
        responsive: number;
        overall: number;
        financial: number;
        relationship: number;
        readiness: number;
    },
    treatmentPlan: null;
    playbook: null;
    financeInfo: {
        balance: number;
        overdue: boolean;
        overdueDays: number;
        isCreditCardAttached: boolean;
        isFinancialPolicySigned: boolean;
        isMedicalConsentSigned: boolean;
    },
    notes: string;
}
