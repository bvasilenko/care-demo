export interface Score {
    overall: number;
    relationship: number;
    responsive: number;
    financial: number;
    readiness: number;
}