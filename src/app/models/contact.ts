export type ContactType = 'work_phone' | 'cell_phone' | 'email';

export interface ContactPhone {
    type: ContactType;
    phone: string;
}

export interface ContactEmail {
    type: ContactType;
    email: string;
}

export type Contact = ContactPhone | ContactEmail;
