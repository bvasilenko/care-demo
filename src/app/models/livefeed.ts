export interface LiveFeed {
    id: number;
    message: string;
    client: {
        id: number;
        name: string;
    },
    time: Date;
}