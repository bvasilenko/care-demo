export namespace PatientDetailsResponse {
    export interface Patient {
        phone_number: string;
        responsiblePartyId: number;
        firstname: string;
        middlename: string;
        lastname: string;
        middleinitial: string;
        sex: string;
        address: {
            country: string;
            state: string;
            zipcode: string;
            full_address: string;
            city: string;
        },
        intelliRank: {
            responsive: number;
            overall: number;
            financial: number;
            relationship: number;
            readiness: number;
        },
        id: number;
        created: string;
        age: number;
        hoh: boolean;
        insuranceplan_primary: InsurancePlan;
        insuranceplan_secondary: InsurancePlan;
        lastvisit: string;
        email: string;
        treatmentplans: TreatmentPlan[];
    }
    export interface InsurancePlan {
        resetmonth: number;
        created: string;
        remainingdeductible: number;
        employer: {
            groupnumber: number;
            insurance_name: string;
            created: string;
            id: number;
            name: string;
        },
        is_secondary: boolean;
        remainingbenefits: number;
        id: number;
    }
    export interface TreatmentPlan {
        totalcost: number;
        updated: string;
        name: string;
        created: string;
        services: Service[],
        id: number;
    }
    export interface Service {
        updated: string;
        fee: number;
        description: string;
        created: string;
        servicecode: string;
        phase: number;
        id: number;
    }
}