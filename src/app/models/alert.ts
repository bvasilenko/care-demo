export interface Alert {
    id: number;
    type: {
        type_id: number;
        type_name: string;
    };
    message: string;
    client?: {
        id: number;
        name: string;
    };
    deadline_date: string;
}