export interface DatePagination {
    fromDate?: number;
    limit: number;
}
