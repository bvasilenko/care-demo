export interface Sorting {
    sortBy: string;
    descending: boolean;
}
