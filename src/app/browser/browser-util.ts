import {Injectable} from '@angular/core';

@Injectable()
export class BrowserUtil {
    
    getDocumentScrollHeight(document: Document) {
        return Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight);
    }

    getWindowHeight(window: Window) {
        return window.document.documentElement.clientHeight || window.innerHeight;
    }

    getWindowScrollY(window: Window) {
        return window.pageYOffset || window.document.documentElement.scrollTop;
    }

    relativeScrollY(window: Window, offset: number) {
        let currentY = this.getWindowScrollY(window);
        window.scrollTo(0, currentY + offset);
    }

}