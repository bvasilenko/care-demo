import { Injectable } from '@angular/core';

@Injectable()
export class WindowRef {

    get window() {
        return window;
    }
    
}