import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BrowserUtil } from './browser-util';
import { DocumentRef } from './document-ref';
import { WindowRef } from './window-ref';

const BOTTOM_OFFSET = 300;

export interface ScrollInfo {
    top: number,
    isScrolledToBottom: boolean;
}

@Injectable()
export class ScrollService {

    private _attached = false;
    private _scroll = new Subject<ScrollInfo>();

    get scroll() {
        return this._scroll.asObservable();
    }

    constructor(
        private _window: WindowRef,
        private _document: DocumentRef,
        private _util: BrowserUtil,
    ) {}

    scrollToBottom() {
        this._window.window.scrollTo(0, this._util.getDocumentScrollHeight(this._document.document));
    }

    isScrolledToBottom() {
        return this._util.getWindowScrollY(this._window.window) >= 
            this._util.getDocumentScrollHeight(this._document.document) - this._util.getWindowHeight(this._window.window) - BOTTOM_OFFSET;
    }

    attach() {
        if (this._attached) {
            return;
        }

        this._window.window.addEventListener('scroll', this._onScroll.bind(this));
    }

    detach() {
        if (!this._attached) {
            return;
        }

        this._window.window.removeEventListener('scroll', this._onScroll.bind(this));
    }

    private _onScroll(e: Event) {
        if (!this._scroll) {
            return;
        }

        this._scroll.next({
            top: this._util.getWindowScrollY(this._window.window),
            isScrolledToBottom: this.isScrolledToBottom(),
        });
    }

}