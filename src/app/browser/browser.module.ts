import { NgModule } from '@angular/core';
import { BrowserUtil } from './browser-util';
import { DocumentRef } from './document-ref';
import { WindowRef } from './window-ref';
import { ScrollService } from './scroll.service';


@NgModule({
    providers: [
        BrowserUtil,
        DocumentRef,
        WindowRef,
        ScrollService,
    ]
})
export class BrowserModule {}