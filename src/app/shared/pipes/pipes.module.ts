import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MoneyPipe } from './money.pipe';
import { ReplacePipe } from './replace.pipe';

const DIRECTIVES = [
    MoneyPipe,
    ReplacePipe,
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: DIRECTIVES,
    exports: DIRECTIVES,
})
export class PipesModule { }
