import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'money' })
export class MoneyPipe implements PipeTransform {

    transform(value: number): string {
        var newValue: any = value;
        if (Math.abs(value) >= 1000) {
            var suffixes = ["", "k", "m", "b", "t"];
            var suffixNum = Math.floor((("" + value.toFixed(0)).replace('-', '').length - 1) / 3);
            var shortValue: any = '', shortNum;
            for (var precision = 2; precision >= 1; precision--) {
                shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(precision));
                var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '');
                if (dotLessShortValue.length <= 2) { break; }
            }
            if (shortValue % 1 != 0) { shortNum = shortValue.toFixed(1); }
            newValue = shortValue + suffixes[suffixNum];
        } else {
            newValue = value.toFixed(0);
        }
        return value >= 0
            ? `$${newValue}`
            : `-$${newValue.toString().replace('-', '')}`;
    }
}