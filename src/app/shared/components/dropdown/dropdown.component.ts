import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'dropdown',
    templateUrl: './dropdown.component.html',
})
export class DropdownComponent {

    @Input() selected: any;
    @Output() selectedChange = new EventEmitter(true);

    private _options: any[];

    get options() {
        return this._options;
    }

    @Input() set options(value: any[]) {
        this._options = value;
        this.selected = value && value[0];
    }

}
