import { Component, Input } from '@angular/core';

@Component({
  selector: 'progress-indicator',
  template: `
    <div class='progress-indicator col-sm-12'>
        <div class='spinner'><i class='fa fa-{{size}} fa-spin fa-refresh'></i></div>
    </div>
    `
})
export class ProgressIndicatorComponent {
    @Input() size = 'lg';
}
