import { AfterContentInit, Component, ContentChildren, Input, EventEmitter, Output, QueryList } from '@angular/core';

import { TabComponent } from './tab.component';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
})
export class TabsComponent implements AfterContentInit {

    @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

    private _activeTabIndex = 0;

    isActiveTab(index) {
        return index === this._activeTabIndex;
    }

    ngAfterContentInit() {
        this.setActiveTab(0);
    }

    setActiveTab(value: number) {
        this._activeTabIndex = value;
        this.tabs.forEach((tab, index) => {
            tab.active = index === value;
        });
    }

}
