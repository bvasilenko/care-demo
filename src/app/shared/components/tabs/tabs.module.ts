import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsComponent } from './tabs.component';
import { TabComponent } from './tab.component';

const COMPONENTS = [
    TabsComponent,
    TabComponent,
];

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS
})
export class TabsModule {}
