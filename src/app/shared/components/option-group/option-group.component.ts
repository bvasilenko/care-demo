import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'option-group',
    templateUrl: './option-group.component.html',
})
export class OptionGroupComponent {

    @Input() selected: any;
    @Output() selectedChange = new EventEmitter(true);

    private _options: any[];


    get options() {
        return this._options;
    }

    @Input() set options(value: any[]) {
        this._options = value;
        this.selected = value && value[0];
    }

}
