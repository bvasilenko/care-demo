import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import { Component, Input, Output, EventEmitter } from '@angular/core';

const DEFAULT_FORMAT = 'DD.MM.YYYY';

@Component({
    selector: 'date-picker',
    templateUrl: './date-picker.component.html',
})
export class DatePickerComponent {

    @Input() value: string;
    @Input() min: number;
    @Input() max: number;
    @Input() format = DEFAULT_FORMAT;
    @Input() alignToRight: boolean;

    @Output() get valueChange() {
        return this._valueChangeDelayed;
    }

    get timestamp() {
        if (!this.value) {
            return;
        }

        const date = moment(this.value);

        if (!date.isValid) {
            return;
        }

        return +date;
    }

    private _valueChange = new Subject<string>();
    private _valueChangeDelayed = this._valueChange.debounceTime(300);

    toDate(timestamp) {
        return timestamp && new Date(timestamp);
    }

    emitChange(newValue: string) {
        this._valueChange.next(newValue);
    }

}
