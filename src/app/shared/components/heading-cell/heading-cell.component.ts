import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Sorting } from 'app/models/core';

@Component({
  selector: 'heading-cell',
  templateUrl: './heading-cell.component.html',
})
export class HeadingCellComponent {

    @Input() sortingKey: string;
    @Input() sorting: Sorting;

    @Output() sortingChange = new EventEmitter(true);

    get isActive() {
        return this.sortingKey === this.sorting.sortBy;
    }

    setSorting(sortBy: string) {
        if (!this.sorting) {
            return;
        }
        const descending = this.isActive ? !this.sorting.descending : false;
        this.sortingChange.emit({sortBy, descending});
    }

}
