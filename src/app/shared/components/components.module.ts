import { InViewportModule } from './in-viewport/in-viewport.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MomentModule } from 'angular2-moment';
import { Ng2DatetimePickerModule } from 'ng2-datetime-picker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { TabsModule } from './tabs/tabs.module';

import { DatePickerComponent } from './date-picker/date-picker.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { HeadingCellComponent } from './heading-cell/heading-cell.component';
import { OptionGroupComponent } from './option-group/option-group.component';
import { StickyComponent } from './sticky/sticky.component';
import { ProgressIndicatorComponent } from './progress-indicator/progress-indicator.component';

const COMPONENTS = [
    DatePickerComponent,
    DropdownComponent,
    HeadingCellComponent,
    OptionGroupComponent,
    StickyComponent,
    ProgressIndicatorComponent,
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MomentModule,
        Ng2DatetimePickerModule,
        BsDropdownModule.forRoot(),
        InViewportModule.forRoot(),

        TabsModule,
    ],
    declarations: COMPONENTS,
    exports: [
        TabsModule,
        InViewportModule,

        ...COMPONENTS,
    ]
})
export class ComponentsModule {}
