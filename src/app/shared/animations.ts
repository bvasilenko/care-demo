import { trigger, state, transition, animate, style } from '@angular/core';

export class Animations {
    static ngIfFadeIn = trigger('ngIfFadeIn', [
        transition(':enter', [
          style({ opacity: 0 }),
          animate('100ms', style({ opacity: 1 }))
        ]),
    ]);
    static ngIfExpand = trigger('ngIfExpand', [
        transition(':enter', [
          style({ opacity: 0, width: '0px' }),
          animate('250ms', style({ opacity: 1, width: '*' }))
        ]),
    ]);
    static collapse = trigger('collapse', [
        state('collapsed', style({ width: '0px' })),
        state('expanded', style({ width: '*' })),
        transition('collapsed => expanded', animate('250ms ease')),
        transition('expanded => collapsed', animate('250ms ease'))
    ]);
}