import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ChartsModule } from 'ng2-charts';
import { NouisliderModule } from 'ng2-nouislider';
import { Ng2DatetimePickerModule } from 'ng2-datetime-picker';
import { MomentModule } from 'angular2-moment';
import { Ng2Webstorage } from 'ng2-webstorage';

import { BrowserModule } from 'app/browser/browser.module';
import { DatasourcesCoreModule } from 'app/datasources/core/datasources-core.module';

import { ComponentsModule } from './components/components.module';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,

        BsDropdownModule.forRoot(),
        ChartsModule,
        MomentModule,
        NouisliderModule,
        Ng2DatetimePickerModule,
        Ng2Webstorage.forRoot({ prefix: 'goldfinger', separator: '.' }),

        BrowserModule,
        DatasourcesCoreModule,
        
        ComponentsModule,
        PipesModule,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,

        BsDropdownModule,
        ChartsModule,
        MomentModule,
        NouisliderModule,
        Ng2DatetimePickerModule,
        Ng2Webstorage,

        ComponentsModule,
        PipesModule,
    ]
})
export class SharedModule {}
