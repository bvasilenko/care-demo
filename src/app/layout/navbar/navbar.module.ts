import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared';
import { ProfileMenuModule } from 'app/layout/profile-menu/profile-menu.module';
import { NavBarComponent } from './navbar.component';

const COMPONENTS = [NavBarComponent];

@NgModule({
    imports: [
        SharedModule,
        ProfileMenuModule,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class NavBarModule {}
