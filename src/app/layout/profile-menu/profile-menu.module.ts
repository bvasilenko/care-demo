import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared';

import { ProfileMenuComponent } from './profile-menu.component';

const COMPONENTS = [ProfileMenuComponent];

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class ProfileMenuModule {}
