import { Component } from '@angular/core';

import { WindowRef } from 'app/browser';

@Component({
    selector: 'profile-menu',
    templateUrl: './profile-menu.component.html'
})
export class ProfileMenuComponent {

    constructor(
        private _window: WindowRef,
    ) {}

    get profile() {
        return {} as any;
    }

    logout() {
        this._window.window.location.reload();
    }

}
