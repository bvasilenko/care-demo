import { ascendingSorter } from './string';

export function sortBy<T, TKey>(
    input: T[], 
    selector: (t: T) => TKey, 
    sorter: (a: TKey, b: TKey) => number = ascendingSorter
) {
    return input.sort((a, b) => sorter(selector(a), selector(b)));
}

export function range(start: number, end: number) {
    const result = [];
    for (let i = start; i < end; i++) {
        result.push(i);
    }
    return result;
}

export function groupBy<T>(
    input: T[], 
    keySelector: (item: T) => string
): {[key: string]: T[]} {
    return input.reduce((result, item) => {
        result[keySelector(item)] = result[keySelector(item)] || [];
        result[keySelector(item)].push(item);
        return result;
    }, {});
}

export function hasIntersection(a: any[], b: any[]) {
    const intersection = a.filter(n => {
        return b.indexOf(n) !== -1;
    });

    return intersection && intersection.length > 0;
}