import { Observable } from 'rxjs';

export function skipNulls<T>(input: Observable<T>) {
    return input.filter(x => !!x);
}