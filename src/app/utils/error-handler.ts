export function errorHandler(error) {
    const message = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    console.error(message);
}

export function errorMockup(error, mockup) {
    console.error(error);
    console.log(
        '=========================='
        + '\nFalling back to API mockup'
        + '\n--------------------------'
        + '\nExpected response:' 
        + `\n${JSON.stringify(mockup, null, 4)}`
        + '\n=========================='
    );
}