export function ascendingSorter(a: any, b: any) {
    var x = a.toString().toLowerCase();
    var y = b.toString().toLowerCase();
    return x < y ? -1 : x > y ? 1 : 0;
}

export function descendingSorter(a: any, b: any) {
    return -1 * ascendingSorter(a, b);
}