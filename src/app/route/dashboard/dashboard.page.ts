import { UnsubscriberFactory, HttpMockupService } from 'app/services';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Kpi } from 'app/models';
import { KpisDatasource } from 'app/datasources';
import { BaseComponent } from 'app/components';
import { skipNulls } from 'app/utils';
import { Status } from 'app/datasources/core';

interface ChartInterval {
    value: string;
    text: string;
}

@Component({
    selector: 'dashboard-page',
    templateUrl: 'dashboard.page.html',
})
export class DashboardPage extends BaseComponent implements OnInit {

    kpisLarge: Array<Kpi.KpiLargeTuple|Kpi.KpiLargeTriple>;
    kpis: Kpi.KpiSimple[];

    intervalOptions = [
        { value: 'days', text: 'day' },
        { value: 'yesterday', text: 'yesterday' },
        { value: 'weeks', text: 'week' },
        { value: 'months', text: 'month' },
    ] as ChartInterval[];

    private _chartLabelFormats = {
        'days': 'MMM Do',
        'yesterday': 'MMM Do',
        'weeks': 'w',
        'months': 'MMM',
    };

    chartSettings = [
        { interval: 'days' },
        { interval: 'days' },
        { interval: 'days' },
        { interval: 'months' },
    ];

    chartColors = [
        {
            color1: '#2fcbec',
            color2: '#84dda7',
        },
        {
            color1: '#d993c1',
            color2: '#6b9fc7',
        },
        {
            color1: '#d993c1',
            color2: '#6b9fc7',
        },
        {
            color1: '#74dfd9',
            color2: '#f1a4a4',
        },
    ];

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _kpisDatasource: KpisDatasource,
        private _sanitizer: DomSanitizer,
        private _http: HttpMockupService,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    get isLoading() {
        return this._kpisDatasource.status === Status.Loading;
    }

    async ngOnInit() {
        this.when(skipNulls(this._kpisDatasource.kpisLarge), x => this.kpisLarge = x);
        this.when(skipNulls(this._kpisDatasource.kpis), x => this.kpis = x);
        this._kpisDatasource.get();
    }

    getChartInterval(index: number) {
        const chartSetting = this.chartSettings[index];
        return chartSetting && this.intervalOptions.find(x => x.value === chartSetting.interval);
    }

    setChartInterval(index: number, option) {
        if (!option || !this.chartSettings[index]) {
            return;
        }
        this.chartSettings[index].interval = option.value;
    }

    getKpiTotal(data: Kpi.KpiSeriesTuple|Kpi.KpiSeriesTriple, chartInterval: ChartInterval) {
        if (!data || !chartInterval) {
            return;
        }

        if (chartInterval.value === 'yesterday') {
            return data.days[data.days.length - 2].total;
        }

        return data[chartInterval.value][data.days.length - 1].total;
    }

    getKpiData(data: Kpi.KpiSeriesTuple|Kpi.KpiSeriesTriple, chartInterval: ChartInterval) {
        if (!data || !chartInterval) {
            return;
        }

        if (chartInterval.value === 'yesterday') {
            return data.days;
        }

        return data[chartInterval.value];
    }

    getChartLabelFormat(chartInterval: ChartInterval) {
        return this._chartLabelFormats[chartInterval.value];
    }

}
