import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared';

import { routing } from './dashboard.routing';

import { DashboardPage } from './dashboard.page';
import { KpiChartComponent } from './kpi-chart.component';
import { KpiSimpleDataComponent } from './kpi-simple-data.component';

const COMPONENTS = [
    DashboardPage,
    KpiChartComponent,
    KpiSimpleDataComponent,
];

@NgModule({
    imports: [
        SharedModule,
        routing,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class DashboardModule { }
