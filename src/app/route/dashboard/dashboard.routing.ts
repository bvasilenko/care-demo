import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { DashboardPage } from './dashboard.page';

const routes: Routes = [
    { path: '', component: DashboardPage }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
