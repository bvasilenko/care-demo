import * as moment from 'moment';
import { Input, Component, OnChanges } from '@angular/core';
import { Kpi } from 'app/models';

@Component({
    selector: 'kpi-chart',
    templateUrl: './kpi-chart.component.html',
})
export class KpiChartComponent implements OnChanges {

    @Input() label1: string;
    @Input() label2: string;
    @Input() color1: string;
    @Input() color2: string;
    @Input() labelFormat = 'MMMM';
    @Input() data: Kpi.KpiValueTuple[];
    @Input() showTotalLine = false;

    chartData;
    chartLabels;
    chartColors;
    chartOptions = {
        scales: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            xAxes: [{
                barThickness: 20,
                stacked: true,
                display: false,
            }],
            yAxes: [{
                stacked: true,
                // ticks: {
                //     display: false,
                // },
                gridLines: {
                    drawBorder: false,
                    // drawTicks: false,
                    borderDash: [5, 2],
                }
            }]
        },
        legend: {
            position: 'bottom',
            labels: null,
        }
    };

    ngOnChanges() {
        this._refresh();
    }

    private _refresh() {
        this.chartOptions.legend.labels = this.showTotalLine ? {
            boxWidth: 12,
            filter: (legendItem, chartData) => legendItem.datasetIndex !== 0,
        } : {
            boxWidth: 12,
        };
        this.chartLabels = this.data.map(x => moment(x.date).format(this.labelFormat));

        // Workaround for https://github.com/valor-software/ng2-charts/issues/692
        setTimeout(() => {
            this.chartOptions.legend.labels = this.showTotalLine ? {
                boxWidth: 12,
                filter: (legendItem, chartData) => legendItem.datasetIndex !== 0,
            } : {
                boxWidth: 12,
            };
            this.chartLabels = this.data.map(x => moment(x.date).format(this.labelFormat));
        }, 0);

        this.chartColors = this.showTotalLine ? [
            { borderWidth: 0 },
            { backgroundColor: this.color1, borderWidth: 0 },
            { backgroundColor: this.color2, borderWidth: 0 },
        ] : [
            { backgroundColor: this.color1, borderWidth: 0 },
            { backgroundColor: this.color2, borderWidth: 0 },
        ];
        this.chartData = this.showTotalLine ? [
            {
                type: 'line',
                fill: false,
                label: this.label2,
                borderColor: '#7b7b7b',
                pointBackgroundColor: '#7b7b7b',
                pointBorderColor: '#7b7b7b',
                lineTension: 0,
                pointRadius: 5,
                data: this.data.map(x => x.value1 + x.value2),
            },
            {
                label: this.label1,
                data: this.data.map(x => x.value1),
            },
            {
                label: this.label2,
                data: this.data.map(x => x.value2),
            },
        ] : [
            {
                label: this.label1,
                data: this.data.map(x => x.value1),
            },
            {
                label: this.label2,
                data: this.data.map(x => x.value2),
            },
        ];
    }

}