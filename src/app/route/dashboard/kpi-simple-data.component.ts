import { Component, Input} from '@angular/core';
import { Kpi } from 'app/models';

@Component({
    selector: 'kpi-simple-data',
    templateUrl: './kpi-simple-data.component.html',
})
export class KpiSimpleDataComponent {

    @Input() cssClass: string;
    @Input() data: Kpi.KpiValue;

}