import { Component, Input, OnInit } from '@angular/core';
import { Score } from 'app/models';
import { Animations } from 'app/shared';

@Component({
    selector: 'score',
    templateUrl: './score.component.html',
    animations: [ Animations.ngIfExpand ],
})
export class ScoreComponent implements OnInit {

    @Input() animate = false;
    @Input() score: Score;

    isExpanded = true;

    ngOnInit() {
        if (this.animate) {
            this.isExpanded = false;
            setTimeout(() => {
                this.isExpanded = true;
            });
        }
    }

}