import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared';

import { routing } from './patients.routing';

import { PatientsPage } from './patients.page';
import { ScoreComponent } from './score.component';
import { OverallScoreCellComponent } from './overall-score-cell.component';
import { TreatmentCellComponent } from './treatment-cell.component';

import { PatientFilterModule } from './filter/patient-filter.module';

const COMPONENTS = [
    PatientsPage,
    ScoreComponent,
    OverallScoreCellComponent,
    TreatmentCellComponent,
];

@NgModule({
    imports: [
        SharedModule,
        routing,

        PatientFilterModule,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class PatientsModule { }
