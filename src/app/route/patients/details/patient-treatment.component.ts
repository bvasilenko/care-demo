import { ChangeDetectorRef, Component, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TreatmentPlan, TreatmentPlanItem } from 'app/models';
import { ObjectMap } from 'app/utils';

@Component({
    selector: 'patient-treatment',
    templateUrl: './patient-treatment.component.html',
})
export class PatientTreatmentComponent implements OnChanges {

    @Input() treatmentPlans: TreatmentPlan[];

    plans: any[];
    plan: TreatmentPlan;

    ngOnChanges() {
        if (this.treatmentPlans) {
            this.plans = this.treatmentPlans.map((plan, i) => Object.assign(plan, {text: `Plan ${i+1}: ${plan.name}`}));
            this.setPlan(this.treatmentPlans[0]);
        }
    }

    setPlan(treatmentPlan: TreatmentPlan) {
        this.plan = treatmentPlan;
    }

}
