import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PatientDetailsPage } from './patient-details.page';

const routes: Routes = [
    { path: '', component: PatientDetailsPage }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
