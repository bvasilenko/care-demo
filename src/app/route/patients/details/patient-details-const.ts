export const RELATIVE_DATE_FORMAT = {
    sameDay: '[Today]',
    nextDay: '[Tomorrow]',
    nextWeek: 'dddd',
    lastDay: '[Yesterday]',
    lastWeek: '[Last] dddd',
    sameElse: ' ',
};

export const DATE_FORMAT = 'h:mma';
