import { ScrollService } from 'app/browser';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BaseComponent } from 'app/components';
import { Status } from 'app/datasources/core';
import { PatientDetailsDatasource } from 'app/datasources';
import { Contact, Patient } from 'app/models';
import { UnsubscriberFactory } from 'app/services';
import { groupBy, ObjectMap, skipNulls } from 'app/utils';

const PHONE_NUMBER_PATTERN = /^\+?(\d+)?(\d{3})(\d{3})(\d{4})$/;
const PHONE_NUMBER_FORMAT = '+1 ($2) $3-$4';

@Component({
    selector: 'patient-details-page',
    templateUrl: './patient-details.page.html',
})
export class PatientDetailsPage extends BaseComponent implements OnInit {

    phoneNumberPattern = PHONE_NUMBER_PATTERN;
    phoneNumberFormat = PHONE_NUMBER_FORMAT;

    patientDetails: Patient;
    contactsByType: ObjectMap<Contact[]>;

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _activatedRoute: ActivatedRoute,
        private _patientDetailsDatasource: PatientDetailsDatasource,
        private _scrollService: ScrollService,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    get isLoading() {
        return this._patientDetailsDatasource.status !== Status.Ready;
    }

    ngOnInit() {
        this.when(this._activatedRoute.params, params => this.onRouteParams(params));
        this.when(skipNulls(this._patientDetailsDatasource.patientDetails), patientDetails => this.onPatientDetails(patientDetails));

        this._scrollService.attach();
    }

    onRouteParams({id}: any) {
        this._patientDetailsDatasource.get(id);
    }

    onPatientDetails(patientDetails: Patient) {
        this.patientDetails = patientDetails;
        this.contactsByType = groupBy(patientDetails.contacts, c => c.type);
    }

}
