import { ScrollService, ScrollInfo } from 'app/browser';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BaseComponent } from 'app/components';
import { Status } from 'app/datasources/core';
import { PatientTasksDatasource } from 'app/datasources';
import { UnsubscriberFactory } from 'app/services';
import { skipNulls } from 'app/utils';

import { RELATIVE_DATE_FORMAT } from './patient-details-const';

const PAGE_SIZE = 6;

@Component({
    selector: 'patient-tasks',
    templateUrl: './patient-tasks.component.html',
})
export class PatientTasksComponent extends BaseComponent implements OnInit, OnChanges {

    @Input() patientId: string;
    relativeDateFormat = RELATIVE_DATE_FORMAT;
    tasks: any[];

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _patientTasksDatasource: PatientTasksDatasource,
        private _scrollService: ScrollService,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    get isLoading() {
        return !this.hasPatientId || this._patientTasksDatasource.status === Status.Loading;
    }

    get isLoadingMore() {
        return this._patientTasksDatasource.status === Status.Updating;
    }

    get hasMore() {
        return this._patientTasksDatasource.hasMore;
    }

    get hasPatientId() {
        return this.patientId != null;
    }

    ngOnInit() {
        this.when(this._scrollService.scroll, e => this.onScroll(e));
    }

    ngOnChanges() {
        if (this.hasPatientId) {
            this.when(skipNulls(this._patientTasksDatasource.tasks), tasks => this._onTasks(tasks));
            this._patientTasksDatasource.getPage(this.patientId, { limit: PAGE_SIZE });
        }
    }

    onScroll(e: ScrollInfo) {
        if (this.isLoading || this.isLoadingMore) {
            return;
        }
        if (e.isScrolledToBottom && this.hasMore) {
            this.loadMore();
        }
    }

    loadMore() {
        this._patientTasksDatasource.getPage(this.patientId, { fromDate: this._patientTasksDatasource.lastTimestamp, limit: PAGE_SIZE });
    }

    private _onTasks(tasks: any[]) {
        this.tasks = tasks;
    }

}
