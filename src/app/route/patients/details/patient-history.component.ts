import { ScrollService, ScrollInfo } from 'app/browser';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BaseComponent } from 'app/components';
import { Status } from 'app/datasources/core';
import { PatientHistoryDatasource } from 'app/datasources';
import { UnsubscriberFactory } from 'app/services';
import { skipNulls } from 'app/utils';

import { DATE_FORMAT, RELATIVE_DATE_FORMAT } from './patient-details-const';

const PAGE_SIZE = 6;

@Component({
    selector: 'patient-history',
    templateUrl: './patient-history.component.html',
})
export class PatientHistoryComponent extends BaseComponent implements OnInit, OnChanges {

    @Input() patientId: string;
    relativeDateFormat = RELATIVE_DATE_FORMAT;
    dateFormat = DATE_FORMAT;
    tasks: any[];

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _patientHistoryDatasource: PatientHistoryDatasource,
        private _scrollService: ScrollService,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    get isLoading() {
        return !this.hasPatientId || this._patientHistoryDatasource.status === Status.Loading;
    }

    get isLoadingMore() {
        return this._patientHistoryDatasource.status === Status.Updating;
    }

    get hasMore() {
        return this._patientHistoryDatasource.hasMore;
    }

    get hasPatientId() {
        return this.patientId != null;
    }

    ngOnInit() {
        this.when(this._scrollService.scroll, e => this.onScroll(e));
    }

    ngOnChanges() {
        if (this.hasPatientId) {
            this.when(skipNulls(this._patientHistoryDatasource.completedTasks), completedTasks => this._onCompletedTasks(completedTasks));
            this._patientHistoryDatasource.getPage(this.patientId, { limit: PAGE_SIZE });
        }
    }

    onScroll(e: ScrollInfo) {
        if (this.isLoading || this.isLoadingMore) {
            return;
        }
        if (e.isScrolledToBottom && this.hasMore) {
            this.loadMore();
        }
    }

    loadMore() {
        this._patientHistoryDatasource.getPage(this.patientId,
            { fromDate: this._patientHistoryDatasource.lastTimestamp, limit: PAGE_SIZE });
    }

    private _onCompletedTasks(completedTasks: any[]) {
        this.tasks = completedTasks;
    }

}
