import { ScrollService, ScrollInfo } from 'app/browser';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BaseComponent } from 'app/components';
import { Status } from 'app/datasources/core';
import { PatientActivitiesDatasource } from 'app/datasources';
import { UnsubscriberFactory } from 'app/services';
import { skipNulls } from 'app/utils';

import { DATE_FORMAT, RELATIVE_DATE_FORMAT } from './patient-details-const';

const PAGE_SIZE = 6;

@Component({
    selector: 'patient-activities',
    templateUrl: './patient-activities.component.html',
})
export class PatientActivitiesComponent extends BaseComponent implements OnInit, OnChanges {

    @Input() patientId: string;
    relativeDateFormat = RELATIVE_DATE_FORMAT;
    dateFormat = DATE_FORMAT;
    activities: any[];

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _patientActivitiesDatasource: PatientActivitiesDatasource,
        private _scrollService: ScrollService,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    get isLoading() {
        return !this.hasPatientId || this._patientActivitiesDatasource.status === Status.Loading;
    }
    
    get isLoadingMore() {
        return this._patientActivitiesDatasource.status === Status.Updating;
    }

    get hasMore() {
        return this._patientActivitiesDatasource.hasMore;
    }

    get hasPatientId() {
        return this.patientId != null;
    }

    ngOnInit() {
        this.when(this._scrollService.scroll, e => this.onScroll(e));
    }

    ngOnChanges() {
        if (this.hasPatientId) {
            this.when(skipNulls(this._patientActivitiesDatasource.activities), activities => this._onActivities(activities));
            this._patientActivitiesDatasource.getPage(this.patientId, { limit: PAGE_SIZE });
        }
    }

    onScroll(e: ScrollInfo) {
        if (this.isLoading || this.isLoadingMore) {
            return;
        }
        if (e.isScrolledToBottom && this.hasMore) {
            this.loadMore();
        }
    }

    loadMore() {
        this._patientActivitiesDatasource.getPage(this.patientId,
            { fromDate: this._patientActivitiesDatasource.lastTimestamp, limit: PAGE_SIZE });
    }

    private _onActivities(activities: any[]) {
        this.activities = activities;
    }

}
