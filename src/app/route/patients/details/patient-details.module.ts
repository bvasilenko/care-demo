import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared';

import { routing } from './patient-details.routing';

import { PatientDetailsPage } from './patient-details.page';
import { PatientActivitiesComponent } from './patient-activities.component';
import { PatientHistoryComponent } from './patient-history.component';
import { PatientTasksComponent } from './patient-tasks.component';
import { PatientTreatmentComponent } from './patient-treatment.component';

const COMPONENTS = [
    PatientDetailsPage,
    PatientActivitiesComponent,
    PatientHistoryComponent,
    PatientTasksComponent,
    PatientTreatmentComponent,
];

@NgModule({
    imports: [
        SharedModule,
        routing,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class PatientDetailsModule { }
