import { Component, Input, HostListener, EventEmitter, OnInit, Output } from '@angular/core';
import { Score } from 'app/models';
import { Animations } from 'app/shared';

@Component({
    selector: 'overall-score-cell',
    templateUrl: './overall-score-cell.component.html',
    animations: [ Animations.collapse, Animations.ngIfFadeIn ],
})
export class OverallScoreCellComponent implements OnInit {

    @Input() score: Score;
    @Input() isExpanded = false;
    @Output() isExpandedChange = new EventEmitter(true);

    isActive = false;

    ngOnInit() {
        if (this.isExpanded) {
            this.isActive = true;
        }
    }

    setActive(e: MouseEvent) {
        this.isActive = true;
        e.stopPropagation();
    }

    @HostListener('document:mouseover')
    onDocumentMouseOver() {
        if (!this.isExpanded) {
            this.isActive = false;
        }
    }

    toggleExpanded() {
        this.isExpanded = !this.isExpanded;

        if (this.isExpanded) {
            this.isExpandedChange.emit(true);
        }
    }

    onExpandAnimationDone() {
        if (!this.isExpanded) {
            this.isExpandedChange.emit(false);
        }
    }

}