import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PatientsPage } from './patients.page';

const routes: Routes = [
    { path: '', component: PatientsPage }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
