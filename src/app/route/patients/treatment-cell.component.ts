import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TreatmentPlanItem } from 'app/models';
import { Animations } from 'app/shared';

@Component({
    selector: 'treatment-cell',
    templateUrl: './treatment-cell.component.html',
    animations: [ Animations.ngIfFadeIn ],
})
export class TreatmentCellComponent {

    @Input() items: TreatmentPlanItem[];
    @Input() isExpanded = false;
    @Output() isExpandedChange = new EventEmitter(true);

}