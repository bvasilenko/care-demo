import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared';

import { PatientFilterComponent } from './patient-filter.component';

const COMPONENTS = [
    PatientFilterComponent,
];

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class PatientFilterModule { }
