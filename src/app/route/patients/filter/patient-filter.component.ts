import * as moment from 'moment';
import { Component, Input, Output, EventEmitter, ChangeDetectorRef, OnInit } from '@angular/core';
import { BaseComponent } from 'app/components';
import { PlaybooksDatasource } from 'app/datasources';
import { PatientFilter, Playbook } from 'app/models';
import { UnsubscriberFactory } from 'app/services';
import { clone, skipNulls } from 'app/utils';

const DATE_FORMAT = 'DD.MM.YYYY';
const TREATMENT_COST_MIN = 0;
const TREATMENT_COST_MAX = 8000;

const RANGES = {
    TODAY: {
        from: +moment().startOf('day'),
        to: +moment().endOf('day'),
    },
    CURRENT_WEEK: {
        from: +moment().startOf('week'),
        to: +moment().endOf('week'),
    },
    CURRENT_MONTH: {
        from: +moment().startOf('month'),
        to: +moment().endOf('month'),
    },
};

@Component({
    selector: 'patient-filter',
    templateUrl: './patient-filter.component.html',
})
export class PatientFilterComponent extends BaseComponent implements OnInit {

    @Input() set filter(value: PatientFilter) {
        this.unsavedFilter = clone(value);
    }

    @Output() filterChange = new EventEmitter(true);
    @Output() close = new EventEmitter(true);

    dateFormat = DATE_FORMAT;
    treatmentCostMin = TREATMENT_COST_MIN;
    treatmentCostMax = TREATMENT_COST_MAX;
    unsavedFilter: PatientFilter;

    patientTypeOptions = [
        { patientType: undefined, text: 'All' },
        { patientType: 'active', text: 'Active' },
        { patientType: 'prospect', text: 'Prospects' },
        { patientType: 'inactive', text: 'Not active' },
    ];

    lastVisitOptions = [
        { lastVisit: undefined, text: 'All' },
        { lastVisit: RANGES.TODAY, text: 'Today' },
        { lastVisit: RANGES.CURRENT_WEEK, text: 'This Week' },
        { lastVisit: RANGES.CURRENT_MONTH, text: 'This Month' },
    ];

    treatmentTypeOptions = [
        { treatmentTypeId: undefined, text: 'All' },
        { treatmentTypeId: 'cracked_tooth', text: 'Cracked Tooth' },
        { treatmentTypeId: 'implant', text: 'Implants' },
    ];

    playbookOptions: any[];

    patientTypeSelected: any;
    lastVisitSelected: any;
    treatmentTypeSelected: any;
    playbookSelected: any;

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _playbooksDatasource: PlaybooksDatasource,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    ngOnInit() {
        this.when(skipNulls(this._playbooksDatasource.playbooks), playbooks => this.onPlaybooks(playbooks));
        this._playbooksDatasource.getAll();
    }

    onPlaybooks(playbooks: Playbook[]) {
        const options = [
            { id: undefined, text: 'All' },
        ];
        this.playbookOptions = options.concat(playbooks.map(p => ({id: p.id, text: p.name})));
    }

    setPatientType(option) {
        this.unsavedFilter.patientType = option.patientType;
        this._saveFilter();
    }

    setLastVisit(option) {
        this.unsavedFilter.lastVisit = option.lastVisit;
        this.lastVisitSelected = option;
        this._saveFilter();
    }

    setLastVisitFrom(value: string) {
        const date = moment(value, this.dateFormat);
        if (!date.isValid()) {
            return;
        }

        this.unsavedFilter.lastVisit = this.unsavedFilter.lastVisit || {} as any;
        this.unsavedFilter.lastVisit.from = +date;

        this.lastVisitSelected = null;

        this._saveFilter();
    }

    setLastVisitTo(value: string) {
        const date = moment(value, this.dateFormat);
        if (!date.isValid()) {
            return;
        }

        this.unsavedFilter.lastVisit = this.unsavedFilter.lastVisit || {} as any;
        this.unsavedFilter.lastVisit.to = +date;

        this.lastVisitSelected = null;

        this._saveFilter();
    }

    setTreatmentType(option) {
        this.unsavedFilter.treatmentTypeId = option.treatmentTypeId;
        this._saveFilter();
    }

    setPlaybook(option) {
        this.unsavedFilter.playbookId = option.id;
        this._saveFilter();
    }

    setTreatmentCost([from, to]) {
        if (from === TREATMENT_COST_MIN && to === TREATMENT_COST_MAX) {
            delete this.unsavedFilter.treatmentCost;
        } else {
            this.unsavedFilter.treatmentCost = this.unsavedFilter.treatmentCost || {} as any;
            this.unsavedFilter.treatmentCost.from = from;
            this.unsavedFilter.treatmentCost.to = to;
        }
        this._saveFilter();
    }

    resetTreatmentCost() {
        this.unsavedFilter.treatmentCost = { from: TREATMENT_COST_MIN, to: TREATMENT_COST_MAX };
    }

    clear() {
        this.unsavedFilter = {} as PatientFilter;
        this.patientTypeSelected = null;
        this.lastVisitSelected = null;
        this.treatmentTypeSelected = this.treatmentTypeOptions[0];
        this.playbookSelected = this.playbookOptions[0];

        this._saveFilter();
    }

    private _saveFilter() {
        this.filterChange.emit(this.unsavedFilter);
        this.detectChanges();
    }

}