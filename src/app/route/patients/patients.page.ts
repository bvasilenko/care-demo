import { Subject } from 'rxjs/Subject';
import { Component, Input, OnInit, ChangeDetectorRef, ViewChild, ElementRef, HostListener, OnDestroy } from '@angular/core';

import { ScrollService, ScrollInfo } from 'app/browser';
import { BaseComponent } from 'app/components';
import { UnsubscriberFactory } from 'app/services';
import { PatientsDatasource } from 'app/datasources';
import { Patient, PatientFilter } from 'app/models';
import { Sorting } from 'app/models/core';
import { Status } from 'app/datasources/core';
import { ObjectMap, skipNulls } from 'app/utils';
import { Animations } from 'app/shared';

const DEFAULT_SORTING = { sortBy: 'fullname', descending: false };
const PAGE_SIZE = 15;

@Component({
    selector: 'patients-page',
    templateUrl: './patients.page.html',
    animations: [ Animations.ngIfFadeIn ],
})
export class PatientsPage extends BaseComponent implements OnInit, OnDestroy {

    patients: Patient[];
    isTreatmentExpanded = {} as ObjectMap<boolean>;
    currentPage = 0;
    totalPages: number;
    isScoreMode: boolean;
    animateScore: boolean;
    tableWidth = 0;

    @ViewChild('table') table: ElementRef;

    isFilterOpen = false;
    filter = {} as PatientFilter;
    searchText: string;
    itemInViewport = {} as ObjectMap<boolean>;
    private _applyFilter = new Subject<void>();
    private _scoreExpanded = {} as ObjectMap<boolean>;

    constructor(
        changeDetector: ChangeDetectorRef,
        unsubscriberFactory: UnsubscriberFactory,
        private _patientsDatasource: PatientsDatasource,
        private _scrollService: ScrollService,
    ) {
        super(changeDetector, unsubscriberFactory);
    }

    get isLoading() {
        return this._patientsDatasource.status == Status.Loading;
    }

    get isLoadingMore() {
        return this._patientsDatasource.status === Status.Updating;
    }

    get sorting() {
        return this._patientsDatasource.sorting;
    }

    get hasMore() {
        return this._patientsDatasource.hasMore;
    }

    @HostListener('window:scroll', ['$event'])
    @HostListener('window:resize', ['$event'])
    @HostListener('window:orientationchange', ['$event'])
    onResize(): void {
        if (this.table && this.table.nativeElement.clientWidth) {
            this.tableWidth = this.table.nativeElement.clientWidth;
        }
    }

    ngOnInit() {
        this.when(skipNulls(this._patientsDatasource.patients), patients => this.onPatients(patients));
        this.when(this._applyFilter.debounceTime(500), () => this._setFilter(this._combineFilter(this.searchText, this.filter)));
        this.getPage(DEFAULT_SORTING, this.filter, 0);
        
        this._scrollService.attach();
        this.when(this._scrollService.scroll, e => this.onScroll(e));
        this.onResize();
    }

    onPatients(patients) {
        this.patients = patients;
        this.totalPages = Math.ceil(this._patientsDatasource.count / PAGE_SIZE);

        setTimeout(() => {
            window.scrollBy(0, 1);
        }, 100);
    }

    getPage(sorting: Sorting, filter: PatientFilter, page: number) {
        this._patientsDatasource.getPage(sorting, filter, { offset: page * PAGE_SIZE, limit: PAGE_SIZE});
    }

    loadMore() {
        this.currentPage++;
        this._patientsDatasource.loadMore({ offset: this.currentPage * PAGE_SIZE, limit: PAGE_SIZE});
    }

    onScroll(e: ScrollInfo) {
        if (this.isLoading || this.isLoadingMore) {
            return;
        }
        if (e.isScrolledToBottom && this.hasMore) {
            this.loadMore();
        }
    }

    applyFilter() {
        this._applyFilter.next();
    }

    isScoreExpanded(patientId: string) {
        return this._scoreExpanded[patientId];
    }

    setScoreExpanded(patientId: string, value: boolean) {
        this._scoreExpanded[patientId] = value;
    }

    toggleScoreMode() {
        this.isScoreMode = !this.isScoreMode;

        this.animateScore = true;
        setTimeout(() => {
            this.animateScore = false;
        }, 250);
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this._scrollService.detach();
    }

    private _setFilter(filter: PatientFilter) {
        this.currentPage = 0;
        this.getPage(this._patientsDatasource.sorting, filter, 0);
    }

    private _combineFilter(searchText: string, filter: PatientFilter) {
        filter.searchText = searchText;
        return filter;
    }
}
