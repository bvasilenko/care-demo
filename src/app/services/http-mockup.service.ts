import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import { environment } from 'environments/environment';
import { errorMockup } from 'app/utils';

const OPTIONS_DEFAULTS = { withCredentials: false };

@Injectable()
export class HttpMockupService {

    constructor(
        protected _http: Http,
    ) { }

    async get(url: string, options?: RequestOptionsArgs, mockup?) {
        try {
            return await this._http.get(this._getUrl(url), this._getOptions(options)).toPromise();
        } catch (err) {
            if (!mockup) {
                throw err;
            }
            errorMockup(err, mockup);
            return mockup;
        }
    }

    async post(url: string, body: any, options?: RequestOptionsArgs, mockup?) {
        try {
            return await this._http.post(this._getUrl(url), body, this._getOptions(options)).toPromise();
        } catch (err) {
            if (!mockup) {
                throw err;
            }
            errorMockup(err, mockup);
            return mockup;
        }
    }

    private _getOptions(customOptions: RequestOptionsArgs = {}): RequestOptionsArgs {
        const options = Object.assign({}, OPTIONS_DEFAULTS, customOptions);
        return options;
    }

    private _getUrl(url: string) {
        if (url.match(/^https?/)) {
            return url;
        }
        return `${environment.apiRoot}${url}`;
    }

}
