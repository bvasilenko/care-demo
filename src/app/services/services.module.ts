import { NgModule } from '@angular/core';

import { HttpService } from './http.service';
import { UnsubscriberFactory } from './unsubscriber.service';
import { HttpMockupService } from './http-mockup.service';

@NgModule({
    providers: [
        HttpService,
        HttpMockupService,
        UnsubscriberFactory,
    ],
})
export class ServicesModule {}
