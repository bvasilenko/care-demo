import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import { environment } from 'environments/environment';

const OPTIONS_DEFAULTS = { withCredentials: false };

@Injectable()
export class HttpService {

    constructor(
        private _http: Http,
    ) { }

    get(url: string, options?: RequestOptionsArgs) {
        return this._http.get(this._getUrl(url), this._getOptions(options));
    }

    post(url: string, body: any, options?: RequestOptionsArgs) {
        return this._http.post(this._getUrl(url), body, this._getOptions(options));
    }

    private _getOptions(customOptions: RequestOptionsArgs = {}): RequestOptionsArgs {
        const options = Object.assign({}, OPTIONS_DEFAULTS, customOptions);
        return options;
    }

    private _getUrl(url: string) {
        if (url.match(/^https?/)) {
            return url;
        }
        return `${environment.apiRoot}${url}`;
    }

}
