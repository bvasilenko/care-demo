const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const firstNames = require('./node_modules/random-name/first-names.json')
const lastNames = require('./node_modules/random-name/names.json')

const app = express();

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'dist')));

function hash(a) {
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    if( a < 0 ) a = 0xffffffff + a;
    return a;
}

function randomInt(max, seed) {
    const result = hash(seed);
    return max ? result % max : result;
}

function _getRandomFirstName(seed) {
    return firstNames[randomInt(seed) % firstNames.length];
}

function _getRandomLastName(seed) {
    return lastNames[randomInt(seed) % lastNames.length];
}

function _getRandomSex(seed) {
    const values = ['F', 'M'];
    return values[randomInt(seed) % values.length];
}

function _getRandomPatientType(seed) {
    const values = ['active', 'prospect', 'inactive'];
    return values[randomInt(seed) % values.length];
}

function _generateTreatmentPlan(seed, id) {
    return {
        "totalCost": randomInt(10000, seed ^ id),
        "name": `Plan #${id+1}`,
        "id": id,
        "phases": [{
            phase: 'A',
            items: [{serviceCode:'Prepare'}, {serviceCode:'Act'}, {serviceCode:'Test'}],
            subtotal: randomInt(3000, seed ^ id),
        }],
    };
}

function _generatePatient(seed) {
    return {
        patientType: _getRandomPatientType(seed),
        firstName: _getRandomFirstName(seed),
        treatmentPlans: [0,1,2].map(id => _generateTreatmentPlan(seed, id)),
        // hoh: obj.hoh,
        sex: _getRandomSex(seed),
        age: randomInt(75, seed),
        insuranceInfo: {
            // remainingBenefits: obj.insuranceplan_primary.remainingbenefits,
            // remainingDeductible: obj.insuranceplan_primary.remainingdeductible,
            // insuranceCompanyName: obj.insuranceplan_primary.employer.insurance_name,
            // groupNumber: obj.insuranceplan_primary.employer.groupnumber,
            // resetMonth: obj.insuranceplan_primary.resetmonth,
            // employerName: obj.insuranceplan_primary.employer.name,
        },
        lastVisit: Date.now() - randomInt(10000, seed) * 3600000,
        // middleName: obj.middlename,
        // responsiblePartyId: obj.responsiblePartyId,
        intelliRank: randomInt(100, seed ^ 10),
        // middleInitial: obj.middleinitial,
        address: {},
        id: randomInt(10000, seed),
        lastName: _getRandomLastName(seed),
        score: {
            overall: randomInt(100, seed ^ 10),
            relationship: randomInt(100, seed ^ 2),
            responsive: randomInt(100, seed ^ 3),
            financial: randomInt(100, seed ^ 4),
            readiness: randomInt(100, seed ^ 5),
        },
        // treatmentPlan: null,
        // playbook: null,
        financeInfo: {
            balance: randomInt(10000, seed),
            overdue: false,
            overdueDays: randomInt(21, seed),
            isCreditCardAttached: true,
            isFinancialPolicySigned: true,
            isMedicalConsentSigned: false
        },
        notes: "Very friendly person who likes sports",
    };
}

function ascendingSorter(a, b) {
    var x = typeof a === 'number' ? a : a.toString().toLowerCase();
    var y = typeof b === 'number' ? b : b.toString().toLowerCase();
    return x < y ? -1 : x > y ? 1 : 0;
}

function descendingSorter(a, b) {
    return -1 * ascendingSorter(a, b);
}

function sortBy(
    input, 
    selector, 
    sorter = ascendingSorter
) {
    return input.sort((a, b) => sorter(selector(a), selector(b)));
}

const TOTAL_COUNT = 1000;
const PATIENT_DATA = [];
for (let i = 0; i < TOTAL_COUNT; i++) {
    PATIENT_DATA.push(_generatePatient(i * 8));
}

app.get('/api/patient/:id', function(req, res) {
    res.send(PATIENT_DATA.find(x => x.id === Number(req.params.id)));
});

app.post('/api/patients', function(req, res) {

    const {
        offset,
        limit,
        sortby,
        sortdesc,
        filter,
    } = req.body;
    
    console.log('/patients: ', {
        offset,
        limit,
        sortby,
        sortdesc,
        filter,
    });

    let data = PATIENT_DATA.slice();

    sortBy(data, x => {
        if (sortby === 'fullname') {
            return `${x.firstName} ${x.lastName}`;
        }
        if (sortby === 'treatmentCost') {
            return Number(x.treatmentPlans[0].totalCost);
        }
        if (sortby === 'score.overall') {
            return Number(x.score.overall);
        }
        if (sortby === 'score.relationship') {
            return Number(x.score.relationship);
        }
        if (sortby === 'score.responsive') {
            return Number(x.score.responsive);
        }
        if (sortby === 'score.financial') {
            return Number(x.score.financial);
        }
        if (sortby === 'score.readiness') {
            return Number(x.score.readiness);
        }
        if (sortby === 'lastvisit') {
            return Number(x.lastVisit);
        }
        return x[sortby] || x.id;
    }, sortdesc ? descendingSorter : ascendingSorter);

    if (filter.searchText) {
        data = data.filter(x => `${x.firstName} ${x.lastName}`.match(filter.searchText));
    }

    if (filter.patientType) {
        data = data.filter(x => x.patientType === filter.patientType);
    }

    if (filter.lastVisit) {
        data = data.filter(x => filter.lastVisit.from <= x.lastVisit && x.lastVisit < filter.lastVisit.to);
    }
    if (filter.treatmentCost) {
        data = data.filter(x => filter.treatmentCost.from <= x.treatmentPlans[0].totalCost && x.treatmentPlans[0].totalCost < filter.treatmentCost.to);
    }

    res.send({
        data: data.slice(offset, offset + limit),
        count: TOTAL_COUNT,
    });
});

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.PORT || '3001';
app.set('port', port);
http.createServer(app).listen(port, () => console.log(`API running on ${port}`));